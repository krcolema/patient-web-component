import babel from '@rollup/plugin-babel';
import resolve from '@rollup/plugin-node-resolve';
import external from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import PeerDepsExternalPlugin from 'rollup-plugin-peer-deps-external';
import typescript from "rollup-plugin-typescript2";

const packageJson = require("./package.json");

export default [
    {
        input: './src/index.js',
        inlineDynamicImports: true,
        output: [
            {
                file: packageJson.main,
                format: 'cjs'
            },
            {
                file: packageJson.module,
                format: 'es',
                exports: 'named'

            }],
        plugins: [
            PeerDepsExternalPlugin(),
            babel({
                exclude: 'node_modules/**',
                presets: ['@babel/preset-flow', '@babel/preset-react'],
                babelHelpers: "bundled",
                extensions: [".js", ".jsx", ".ts", ".tsx"]
            }),
            postcss(),

            commonjs(),
            typescript({ useTsconfigDeclarationDir: true }),
            external(),
            resolve({
                extensions: [".js", ".jsx", ".ts", ".tsx"],
                resolveOnly: [
                    /^(?!react$)/,
                    /^(?!react-dom$)/,
                    /^(?!prop-types)/,
                ]
            }),
            terser()
        ]
    }
];