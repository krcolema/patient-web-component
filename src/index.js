import { PatientInfoCard } from './components/PatientInfoCard';
import { TestInfoCard } from './components/TestInfoCard';

export {
    PatientInfoCard,
    TestInfoCard
}

// export * from './components/PatientInfoCard';
// export * from './components/TestInfoCard';