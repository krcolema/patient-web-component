import React from 'react';
import { PatientInfoCard } from '../components/PatientInfoCard';


export default {
    title: 'Patient/PatientInfoCard',
    component: PatientInfoCard,
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
    argTypes: {
        backgroundColor: { control: 'color' },
    },
};

const Template = (args) => <PatientInfoCard {...args} />;

export const Patient1 = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Patient1.args = {
    firstName: 'Test',
    lastName: 'Patient1',
    admissionStatus: 'Admitted',
    patientId: '14785',
    patientCode: 'XF9328',
    sex: 'male',
    age: '32',
    socDate: '10/18/2021',
};

export const Patient2 = Template.bind({});

Patient2.args = {
    firstName: 'Test',
    lastName: 'Patient2',
    admissionStatus: 'Pre-Admit',
    patientId: '95215',
    patientCode: '82C124',
    sex: 'male',
    age: '31',
    socDate: '05/04/2017',
};

export const Patient3 = Template.bind({});

Patient3.args = {
    firstName: 'Test',
    lastName: 'Patient3',
    admissionStatus: 'Admitted',
    patientId: '21564',
    patientCode: 'KSI797',
    sex: 'female',
    age: '31',
    socDate: '01/15/2020',
};

export const Patient4 = Template.bind({});

Patient4.args = {
    firstName: 'Test',
    lastName: 'Patient4',
    admissionStatus: 'Admitted',
    patientId: '87521',
    patientCode: 'F4N932',
    sex: 'female',
    age: '43',
    socDate: '01/05/2022',
};