import React from 'react';
import './PatientInfoCard.css';
import PropTypes from 'prop-types';


export const PatientInfoCard = ({ firstName, lastName, admissionStatus, patientId, patientCode, sex, age, socDate, backgroundColor }) => {
    return (
        <div className="patientInfoCardContainer"
            style={backgroundColor && { backgroundColor }}
        >
            <div className='grid'>
                <div className='row'>
                    <div className='column'>
                        <span className='patientName'>{firstName} {lastName}</span> <span className='admissionStatus'>{admissionStatus}</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='column'>
                        {patientId !== '' ? patientId : patientCode} {sex} {age !== '' ? '| ' + age : ''}
                    </div>
                </div>
                <div className='row'>
                    <div className='column'>
                        {socDate !== '' ? 'Start Date ' + socDate : ''}
                    </div>
                </div>
            </div>
        </div>
    );
};


PatientInfoCard.propTypes = {
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    admissionStatus: PropTypes.string,
    patientId: PropTypes.string,
    patientCode: PropTypes.string,
    sex: PropTypes.string,
    age: PropTypes.string,
    socDate: PropTypes.string,
    backgroundColor: PropTypes.string,
};

PatientInfoCard.defaultProps = {
    firstName: 'Test',
    lastName: 'Patient1',
    admissionStatus: 'Admitted',
    patientId: '14785',
    patientCode: '89773',
    sex: 'male',
    age: '32',
    socDate: '10/18/2021',
    backgroundColor: null
}
     