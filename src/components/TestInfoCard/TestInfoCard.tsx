import { IonCard, IonCol, IonGrid, IonRow } from '@ionic/react';
import React from 'react';
import './TestInfoCard.scss';

export interface TestInfoCardProps {
    firstName: string;
    lastName: string;
    admissionStatus: string;
    patientId: string;
    patientCode: string;
    sex: string;
    age: string;
    socDate: string;
}


const TestInfoCard: React.FC<TestInfoCardProps> = ({ firstName, lastName, admissionStatus, patientId, patientCode, sex, age, socDate }) => {
    return (
        <IonCard className="patientInfoCardContainer">
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <span className='patientName'>{firstName} {lastName}</span> <span className='admissionStatus'>{admissionStatus}</span>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        {patientId !== '' ? patientId : patientCode} {sex} {age !== '' ? '| ' + age : ''}
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        {socDate !== '' ? 'Start Date ' + socDate : ''}
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonCard>
    );
};

TestInfoCard.displayName = 'TestInfoCard';

export default TestInfoCard;

