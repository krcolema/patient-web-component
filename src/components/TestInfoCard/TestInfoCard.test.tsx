import React from "react";
import { render } from "@testing-library/react";

import TestInfoCard from "./TestInfoCard";
import { TestInfoCardProps } from "./TestInfoCard";

describe("Test Component", () => {
    let props: TestInfoCardProps;

    beforeEach(() => {
        props = {
            firstName: 'Test',
            lastName: 'Patient1',
            admissionStatus: 'Admitted',
            patientId: '14785',
            patientCode: '89773',
            sex: 'male',
            age: '32',
            socDate: '10/18/2021'
        };
    });

    const renderComponent = () => render(<TestInfoCard {...props} />);

    it("should have a first name with default props", () => {
        const { getByTestId } = renderComponent();

        const testComponent = getByTestId("test-info-card");

        expect(testComponent).toHaveClass("patientInfoCardContainer");
    });
});